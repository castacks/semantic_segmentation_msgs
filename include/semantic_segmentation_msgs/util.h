#ifndef UTIL_H_LY7WJXMT
#define UTIL_H_LY7WJXMT

#include <semantic_segmentation_msgs/LabelsTensor.h>

namespace semantic_segmentation_msgs
{

inline uint32_t get_offset(const LabelsTensor& tensor_msg,
                           int channel, int row, int col) {
  uint32_t offset = (tensor_msg.labels.layout.dim[0].stride*channel +
                     tensor_msg.labels.layout.dim[1].stride*row +
                     tensor_msg.labels.layout.dim[2].stride*col);
  return offset;
}

/**
 * Note this assumes uncompressed data.
 * No checks of any kind are performed.
 */
inline uint8_t get_label(const LabelsTensor& tensor_msg,
                         int channel, int row, int col) {
  uint32_t offset = get_offset(tensor_msg, channel, row, col);
  return tensor_msg.labels.data[offset];
}

inline uint32_t n_channels(const LabelsTensor& tensor_msg) {
  return tensor_msg.labels.layout.dim[0].size;
}

inline uint32_t n_rows(const LabelsTensor& tensor_msg) {
  return tensor_msg.labels.layout.dim[1].size;
}

inline uint32_t n_cols(const LabelsTensor& tensor_msg) {
  return tensor_msg.labels.layout.dim[2].size;
}

class LabelsTensorWrapper {
public:
  LabelsTensorWrapper(const LabelsTensor& tensor_msg) :
      tensor_msg_(tensor_msg)
  { }
  virtual ~LabelsTensorWrapper() { }
  LabelsTensorWrapper(const LabelsTensorWrapper& other) = delete;
  LabelsTensorWrapper& operator=(const LabelsTensorWrapper& other) = delete;

  uint32_t channels() const { return tensor_msg_.labels.layout.dim[0].size; }
  uint32_t rows() const { return tensor_msg_.labels.layout.dim[1].size; }
  uint32_t cols() const { return tensor_msg_.labels.layout.dim[2].size; }

  uint32_t get_offset(int channel, int row, int col) const {
    return (tensor_msg_.labels.layout.dim[0].stride*channel +
            tensor_msg_.labels.layout.dim[1].stride*row +
            tensor_msg_.labels.layout.dim[2].stride*col);
  }

  uint8_t get_label(int channel, int row, int col) const {
    uint32_t ix(this->get_offset(channel, row, col));
    uint8_t lbl = tensor_msg_.labels.data[ix];
    return lbl;
  }

  template<typename IterT>
  void copy_label_channel(int channel, IterT begin_itr) const {
    size_t begin_offset = channel*tensor_msg_.labels.layout.dim[0].stride;
    size_t end_offset = (channel+1)*tensor_msg_.labels.layout.dim[0].stride;
    const uint8_t* start_ptr = &(tensor_msg_.labels.data[begin_offset]);
    const uint8_t* end_ptr = &(tensor_msg_.labels.data[end_offset]);
    std::copy(start_ptr, end_ptr, begin_itr);
  }

private:
  const LabelsTensor& tensor_msg_;
};


} /* semantic_segmentation_msgs */

#endif /* end of include guard: UTIL_H_LY7WJXMT */
