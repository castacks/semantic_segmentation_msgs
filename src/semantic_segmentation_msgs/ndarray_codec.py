"""
>>> import StringIO
>>> import numpy as np
>>> from semantic_segmentation_msgs.msg import NdArray
>>> from semantic_segmentation_msgs import ndarray_codec
>>> a = np.random.random((3, 10, 10))
>>> msg = ndarray_codec.encode(a)
>>> buf = StringIO.StringIO()
>>> msg.serialize(buf)
>>> ser_msg = buf.getvalue()
>>> msg2 = NdArray()
>>> _  = msg2.deserialize(ser_msg) # doctest:+ELLIPSIS
>>> a2 = ndarray_codec.decode(msg2)
>>> np.all(a == a2)
True
"""

import numpy as np

from semantic_segmentation_msgs.msg import NdArray

# from rospy.numpy_msg import numpy_msg
# NdArray = numpy_msg(NdArray)


def encode(arr):
    """ Data encoder for serializing numpy data types.

    >>> import numpy as np
    >>> from semantic_segmentation_msgs.msg import NdArray
    >>> from semantic_segmentation_msgs import ndarray_codec
    >>> a = np.random.random((3, 10, 10))
    >>> msg = ndarray_codec.encode(a)
    """
    msg = NdArray()
    msg.shape = arr.shape
    msg.typestr = arr.dtype.str
    msg.data = arr.tobytes()
    msg.version = 3
    return msg


def decode(msg):
    """ Decoder for deserializing numpy data types.
    """

    return np.fromstring(msg.data, dtype=np.dtype(msg.typestr)).reshape(msg.shape)


if __name__ == "__main__":

    import doctest
    doctest.testmod(verbose=True)
