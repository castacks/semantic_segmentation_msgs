"""
Numpy <-> ROS Multiarray

>>> import numpy as np
>>> from std_msgs.msg import UInt8MultiArray
>>> from std_msgs.msg import Float32MultiArray
>>> from std_msgs.msg import Float64MultiArray
>>> from std_msgs.msg import MultiArrayDimension
>>> from semantic_segmentation_msgs import ndma
>>> a = np.random.random((10, 20, 3)).astype('f4')
>>> ma = ndma.ndarray_to_multiarray(a)
>>> b = ndma.multiarray_to_ndarray(ma)
>>> np.all(a == b)
True
>>> a = np.random.random((10, 20, 3)).astype('f8')
>>> ma = ndma.ndarray_to_multiarray(a)
>>> b = ndma.multiarray_to_ndarray(ma)
>>> np.all(a == b)
True
>>> a = np.random.random((10, 20, 3)).astype('u1')
>>> ma = ndma.ndarray_to_multiarray(a)
>>> b = ndma.multiarray_to_ndarray(ma)
>>> np.all(a == b)
True
>>> a = np.random.random((10, 20, 3)).astype('u1')
>>> ma = ndma.ndarray_to_multiarray(a, ['height', 'width'])
Traceback (most recent call last):
    ...
ValueError: length of labels should equal arr.ndim
>>> ma = ndma.ndarray_to_multiarray(a, ['height', 'width', 'channels'])
>>> ma.layout.dim[2].label
'channels'
>>> ma.layout.dim[2].size
3

"""

import numpy as np

from std_msgs.msg import UInt8MultiArray
from std_msgs.msg import Float32MultiArray
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import MultiArrayDimension

# from rospy.numpy_msg import numpy_msg
# UInt8MultiArray = numpy_msg(UInt8MultiArray)
# Float32MultiArray = numpy_msg(Float32MultiArray)
# Float64MultiArray = numpy_msg(Float64MultiArray)


MULTIARRAY_TO_DTYPE = {
        UInt8MultiArray: np.dtype('uint8'),
        Float32MultiArray: np.dtype('float32'),
        Float64MultiArray: np.dtype('float64'),
        }

DTYPE_TO_MULTIARRAY = dict([(v,k) for (k,v) in MULTIARRAY_TO_DTYPE.items()])


def multiarray_to_ndarray(msg):
    """ MultiArray message to numpy ndarray.
    """
    dtype = MULTIARRAY_TO_DTYPE[type(msg)]
    arr = np.frombuffer(msg.data, dtype=dtype)
    # we will ignore strides for now
    dims = [d.size for d in msg.layout.dim]
    arr2 = arr.reshape(dims)
    return arr2


def ndarray_to_multiarray(arr, labels=None):
    """ python ndarray to multiarray msg.
    """
    if labels is not None and len(labels) != arr.ndim:
        raise ValueError('length of labels should equal arr.ndim')
    MultiArrayType = DTYPE_TO_MULTIARRAY[arr.dtype]
    msg = MultiArrayType()
    # NOTE: ensure contiguous array *before* using strides
    arr = np.ascontiguousarray(arr)
    for di in xrange(arr.ndim):
        lbl = 'dim%d'%di if (labels is None) else labels[di]
        msg.layout.dim.append(MultiArrayDimension(label=lbl, size=arr.shape[di], stride=arr.strides[di]))
    msg.data = np.ascontiguousarray(arr).tostring()
    return msg


if __name__ == "__main__":
    import doctest
    doctest.testmod(verbose=True)
