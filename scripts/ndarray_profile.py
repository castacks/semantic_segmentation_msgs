from __future__ import print_function

import time
from cStringIO import StringIO

import numpy as np

from semantic_segmentation_msgs import ndarray_codec

arr = np.random.random((1024, 1024, 3)).astype('f4')
msg = ndarray_codec.encode(arr)
buf = StringIO()
msg.serialize(buf)
print('msg len: %d'%len(buf.getvalue()))


encs = []
decs = []
for _ in range(100):
    arr = np.random.random((1024, 1024, 3)).astype('f4')
    tic = time.time()
    msg = ndarray_codec.encode(arr)
    encs.append(time.time() - tic)

    tic = time.time()
    arr2 = ndarray_codec.decode(msg)
    decs.append(time.time() - tic)

print('enc: %f'%(np.median(encs)))
print('dec: %f'%(np.median(decs)))
