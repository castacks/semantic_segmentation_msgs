# semantic_segmentation_msgs

Daniel Maturana (dimatura@gmail.com) 2018

ROS messages for semantic segmentation.

## License

BSD, see LICENSE
